<!DOCTYPE html>
<html>
<head>
	<title>Add Category</title>
</head>
<body>
	<h1>Add Category Form</h1>
	<form id="addCategoryForm" enctype="multipart/form-data">
		Name: <input type="text" name="name"><br>
		Description: <input type="text" name="description"><br>
		Price: <input type="number" name="price"><br>
		Stock: <input type="number" name="stock"><br>
		Category: <select name="category" id="listing">
			<option selected>Select Category</option>
		</select><br>
		{{-- Image: <input type="file" name="image"><br> --}}

		<button type="button" id="submitBtn">Add</button>
	</form>

	<script type="text/javascript">
		fetch('http://localhost:3000/categories')
		.then(function(res){
			return res.text();
		})
		.then(function(data){
			console.log(JSON.parse(data));
			let category = JSON.parse(data);
			category.forEach(function(cat){
				listing.innerHTML += '<option value="'+cat.name+'">'+cat.name+'</option>';
			})
		})

		submitBtn.addEventListener('click', function(){
			let formData = new FormData(addCategoryForm);
			let object = {};
			formData.forEach(function(value, name){
				object[name] = value;
			});
			let json = JSON.stringify(object);
			const headers = {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'Access-Controll-Allow-Origin' : '*',
				'Access-Controll-Allow-Methods' : '*'
			};

			fetch('http://localhost:3000/items/add', {
				method : 'post',
				headers: headers,
				body: json

			})
			.then(function(res){
				return res.text();
			})
			.then(function(data){
				console.log(data);
			});
		})
	</script>

</body>
</html>