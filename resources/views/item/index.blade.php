<!DOCTYPE html>
<html>
<head>
	<title>Items Page</title>
</head>
<body>
	<h1>Item List</h1>

	<table>
		<thead>
			<tr>
				<th></th>
				<th>Name</th>
				<th>Description</th>
				<th>Category</th>
				<th>Price</th>
				<th>Stocks</th>
			</tr>
		</thead>
		<tbody id="listing">
			@foreach($items as $item)
				<tr>
					<td></td>
					<td>$item->name</td>
					<td>$item->description</td>
					<td>$item->category</td>
					<td>$item->price</td>
					<td>$item->stock</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<script type="text/javascript">
		// GUZZLED

		// fetch('http://localhost:3000/items')
		// .then(function(res){
		// 	return res.text();
		// })
		// .then(function(data){
		// 	console.log(JSON.parse(data));
		// 	let category = JSON.parse(data);
		// 	category.forEach(function(cat){
		// 		listing.innerHTML += "<tr><td></td><td>"+cat.name+"</td><td>"+cat.description+"</td><td>"+cat.category+"</td><td>"+cat.price+"</td><td>"+cat.stock+"</td></tr>";
		// 	})
		// })
	</script>

</body>
</html>