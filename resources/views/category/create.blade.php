<!DOCTYPE html>
<html>
<head>
	<title>Add Category</title>
</head>
<body>
	<h1>Add Category Form</h1>
	<form id="addCategoryForm">
		Name: <input type="text" name="name"><br>
		<button type="button" id="submitBtn">Add</button>
	</form>

	<script type="text/javascript">
		submitBtn.addEventListener('click', function(){
			let formData = new FormData(addCategoryForm);
			let object = {};
			formData.forEach(function(value, name){
				object[name] = value;
			});
			let json = JSON.stringify(object);
			const headers = {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'Access-Controll-Allow-Origin' : '*',
				'Access-Controll-Allow-Methods' : '*'
			};

			fetch('http://localhost:3000/categories/create', {
				method : 'post',
				headers: headers,
				body: json

			})
			.then(function(res){
				return res.text();
			})
			.then(function(data){
				console.log(data);
			});
		})
	</script>

</body>
</html>