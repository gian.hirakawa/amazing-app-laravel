<!DOCTYPE html>
<html>
<head>
	<title>Category Page</title>
</head>
<body>
	<h1>Categories</h1>

	<ul id="listing">
		@foreach($categories as $cat)
		<li>{{$cat->name}}</li>
		@endforeach
	</ul>

	<script type="text/javascript">
		// commented out, since could fetch with laravel using guzzle

		// fetch('http://localhost:3000/categories')
		// .then(function(res){
		// 	return res.text();
		// })
		// .then(function(data){
		// 	console.log(JSON.parse(data));
		// 	let category = JSON.parse(data);
		// 	category.forEach(function(cat){
		// 		listing.innerHTML += "<li>"+cat.name+"</li>";
		// 	})
		// })
	</script>

</body>
</html>