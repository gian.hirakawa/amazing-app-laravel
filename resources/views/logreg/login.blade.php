<!DOCTYPE html>
<html>
<head>
	<title>Register Page</title>
</head>
<body>

	<form id="registerForm">
		Email: <input type="email" name="email"><br>
		Password: <input type="password" name="password"><br>
		<button id="regBtn" type="button">Login</button>
	</form>

	<script type="text/javascript">
		regBtn.addEventListener('click', function(){
			let formData = new FormData(registerForm);

			let object = {};
			formData.forEach(function(value, name){
				object[name] = value;
			});
			let json = JSON.stringify(object);
			console.log(formData);
			const headers = {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'Access-Controll-Allow-Origin' : '*',
				'Access-Controll-Allow-Methods' : '*'
			};


			fetch('http://localhost:3000/reg/login', {
				method: 'post',
				headers: headers,
				body: json
			})
			.then(function(res){
				return res.text();
			})
			.then(function(data){
				let token = JSON.parse(data);
				localStorage.setItem('secret', token.token);
				console.log(localStorage.getItem('secret'));
			})
		})
	</script>

</body>
</html>