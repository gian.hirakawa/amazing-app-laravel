<!DOCTYPE html>
<html>
<head>
	<title>Register Page</title>
</head>
<body>

	<h1>User Profile</h1>

	<script type="text/javascript">
		let token = localStorage.getItem('secret');
		const headers = {
			'Authorization' : 'Bearer ' + token
		};

		fetch('http://localhost:3000/userprofile', {
				headers: headers,
			})
			.then(function(res){
				return res.text();
			})
			.then(function(data){
				console.log(data);
			})
		
	</script>

</body>
</html>