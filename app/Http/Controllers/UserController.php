<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{
    function register(){

		return view('logreg.register');
	}

	function login(){

		return view('logreg.login');
	}

	function userprofile(){

		return view('logreg.userprofile');
	}
}
