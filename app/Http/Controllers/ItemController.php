<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemController extends Controller
{
    function index(){
    	$client = new Client();
    	$res = $client->get('http://localhost:3000/items');
    	$items = json_decode($res->getBody());
    	// return view('category.index', compact('categories'));
    	return view('item.index', compact('items'));
    }

     function add(){

    	return view('item.add');
    }
}
