<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class CategoryController extends Controller
{
    function index(){
    	$client = new Client();
    	$res = $client->get('http://localhost:3000/categories');
    	$categories = json_decode($res->getBody());
    	return view('category.index', compact('categories'));
    	// return view('category.index');
    }

     function create(){

    	return view('category.create');
    }
}
