<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', 'UserController@register');
Route::get('/login', 'UserController@login');
Route::get('/userprofile', 'UserController@userprofile');

Route::get('/categories', 'CategoryController@index');
Route::get('/categories/create', 'CategoryController@create');

Route::get('/items', 'ItemController@index');
Route::get('/items/add', 'ItemController@add');